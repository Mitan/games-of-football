from django.contrib.auth import *
from django.shortcuts import get_object_or_404
from django import forms
import datetime

from mainApp.models import Match, Team, Stats


class MatchForm(forms.ModelForm):
    id_team1 = forms.CharField(label='Team 1')
    id_team2 = forms.CharField(label='Team 2')
    date = forms.DateField(initial=datetime.date.today)
    class Meta:
        model = Match or Team
        fields = [
            'date',
            'localization'
        ]

class StatsForm1(forms.ModelForm):
    class Meta:
        model = Stats
        fields = [
            'ball_position',
            'passes',
            'fouls',
            'strikes',
            'goals'
        ]

class StatsForm2(forms.ModelForm):
    class Meta:
        model = Stats
        fields = [
            'ball_position',
            'passes',
            'fouls',
            'strikes',
            'goals'
        ]