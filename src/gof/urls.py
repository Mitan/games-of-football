"""gof URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from pages.views import (
    home_view,
)
from player.views import (
    players_view,
    player_detail_view,
    player_update_view
)
from manager.views import (
    manager_view,
    manager_update_view,
    manager_create_view,
    manager_delete_view
)
from organizator.views import (
    organizator_view,
    organizator_update_view,
    organizator_create_view,
    organizator_delete_view,
    tournament_view,
    tournament_list_view,
    tournament_create_view,
    petition_view
)
from team.views import (
    team_view,
    team_list_view,
    team_update_view,
    team_create_view,
    team_registration_view,
    update_registration_view,
    delete_registration_view,
    team_tournament_view,
)
from match.views import (
    match_view,
    match_list_view,
    match_create_view,
    stats_create_view,
    stats_view
)
from panel.views import (
    panel_view,
)

from users import views as user_views
from django.contrib.auth import views as auth_views

urlpatterns = [
    path('team<int:idTeam>/players/', players_view, name='player-list'),
    path('team<int:idTeam>/player<int:id>/', player_detail_view, name='player-detail'),
    path('team<int:idTeam>/player<int:id>/update/', player_update_view, name='player-update'),
    path('team_create/', team_create_view, name='team-create'),

    path('manager<int:id>/', manager_view, name='manager-view'),
    path('manager<int:id>/update/', manager_update_view, name='manager-update'),
    path('manager_create/', manager_create_view, name='manager-create'),
    path('manager<int:id>/delete/', manager_delete_view, name='manager-delete'),
    path('manager<int:id>/team_tournament/', team_tournament_view),

    path('organizator<int:id>/', organizator_view, name='organizator-view'),
    path('organizator<int:id>/update/', organizator_update_view, name='organizator-update'),
    path('organizator_create/', organizator_create_view, name='organizator-create'),
    path('organizator<int:id>/delete/', organizator_delete_view, name='organizator-delete'),

    path('tournament<int:id>/', tournament_view, name='tournament'),
    path('tournamentlist/', tournament_list_view, name='tournament-list'),

    path('update_registration<int:id>', update_registration_view),
    path('delete_registration<int:id>', delete_registration_view),

    path('tournament_create/', tournament_create_view, name='tournament-create'),

    path('team<int:idTeam>/', team_view, name='team-view'),
    path('tournament<int:id>/teams/', team_list_view, name='team-list'),
    path('tournament<int:id>/petitions/', petition_view),
    path('team<int:idTeam>/update', team_update_view, name='team-update'),
    path('team_create/', team_create_view, name='team-create'),

    path('tournament<int:id>/matches/', match_list_view),
    path('tournament<int:id>/match<int:idMatch>/', match_view),
    path('tournament<int:id>/match_create/', match_create_view),
    path('tournament<int:id>/team_registration/', team_registration_view),

    path('tournament<int:id>/match<int:idMatch>/stats_create/', stats_create_view),
    path('tournament<int:id>/match<int:idMatch>/stats/', stats_view),

    path('register/', user_views.register, name='register'),
    path('login/', auth_views.LoginView.as_view(template_name='users/login.html'), name='login'),
    path('logout/', auth_views.LogoutView.as_view(template_name='users/logout.html'), name='logout'),

    path('', home_view),
    path('panel/', panel_view),
    path('admin/', admin.site.urls),
]
