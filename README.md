## Table of Contents
* [General info](#general-info)
* [Technologies](#technologies)
* [Setup](#setup)

## General info
This project is simple Website about football tournaments. You can register as manager as organizer.
As manager you can add your team to tournament, edit it and follow tenor of tournament.
As organizer you can add new tournament, accept/deny teams requests to join your tournament, etc.

## Technologies
* Python 3.8.1
* Django 3.0.3
* HTML5
* CSS3

## Setup
### Linux/MacOS
```
$ source bin/activate
$ cd src
$ python3 manage.py runserver
```

### Windows
```
$ .\Scripts\activate
$ dir src
$ python3 manage.py runserver
```