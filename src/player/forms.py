from django import forms

from mainApp.models import Player, LoginData


class PlayerForm(forms.ModelForm):
    class Meta:
        model = Player
        fields = [
            'name',
            'surname',
            'height',
            'weight',
            'position',
            'overall',
            'better_foot',
        ]

class LoginDataForm(forms.ModelForm):
    class Meta:
        model = LoginData
        fields = [
            'login',
            'password',
            'email'
        ]
