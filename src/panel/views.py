from django.shortcuts import render, redirect
from django.contrib.auth import authenticate

from mainApp.models import Manager, Promoter, LoginData

# Create your views here.
def panel_view(request, *args, **kwargs):
    if request.user.is_authenticated:
        log = LoginData.objects.get(login=request.user.username)
        try:
            objects_manager = Manager.objects.get(id_logindata__exact=log.id_logindata)
        except Manager.DoesNotExist:
            objects_manager = None
        try:
            objects_promoter = Promoter.objects.get(id_logindata__exact=log.id_logindata)
        except Promoter.DoesNotExist:
            objects_promoter = None

        if objects_manager is not None:
            return redirect('/manager'+str(objects_manager.id_manager))
        if objects_promoter is not None:
            return redirect('/organizator'+str(objects_promoter.id_promoter))


        ctx = {
            "objects_manager": objects_manager,
            "objects_promoter": objects_promoter,
        }
        return render(request, "panel.html", ctx)