from django.shortcuts import render, redirect, get_object_or_404
from django.contrib import messages

from .forms import MatchForm, StatsForm1, StatsForm2
from mainApp.models import Match, Team, Tournament, Promoter, LoginData, Manager, Stats

def match_list_view(request, id):
    queryset = Match.objects.filter(id_tournament__exact=id)
    for item in queryset:
        item.id_team1 = Team.objects.get(id_team__exact=item.id_team1).name
        item.id_team2 = Team.objects.get(id_team__exact=item.id_team2).name

    ctx = {
        "objects_list": queryset,
    }
    return render(request, "match-list.html", ctx)

def match_view(request, id, idMatch):
    obj = get_object_or_404(Match, id_match__exact=idMatch)

    obj.id_team1 = Team.objects.get(id_team__exact=obj.id_team1).name
    obj.id_team2 = Team.objects.get(id_team__exact=obj.id_team2).name

    ctx = {
        "idTournament": str(id)
    }
    ctx["objects"] = obj
    obj = get_object_or_404(Tournament, id_tournament=id)

    if request.user.is_authenticated:
        log = LoginData.objects.get(login=request.user.username)

        try:
            organizator = Promoter.objects.get(id_logindata=log.id_logindata)
        except Promoter.DoesNotExist:
            organizator = None
        if organizator:
            ctx["organizator"] = str(organizator.id_tournament)

        
    return render(request, "match.html", ctx)

def match_create_view(request, id):
    form = MatchForm(request.POST or None)
    if form.is_valid() and request.user.is_authenticated:
        id_tournament = Tournament.objects.get(id_tournament__exact=id)
        try:
            team1 = Team.objects.get(name__exact=form.cleaned_data['id_team1']).id_team
        except:
            messages.error(request, f'There is no team ' + form.cleaned_data['id_team1'])
            return redirect('./')

        try:
            team2 = Team.objects.get(name__exact=form.cleaned_data['id_team2']).id_team
        except:
            messages.error(request, f'There is no team ' + form.cleaned_data['id_team2'])
            return redirect('./')

        date = form.cleaned_data['date']
        loc = form.cleaned_data['localization']
        match = Match(id_team1=team1, id_team2=team2, date=date, localization=loc, id_tournament=id_tournament)
        match.save()
        return redirect('../')
    
    ctx = {
        "form": form
    }
    return render(request, "match-create.html", ctx)

def stats_create_view(request, id, idMatch):
    obj = get_object_or_404(Match, id_match__exact=idMatch)
    form1 = StatsForm1(request.POST or None)
    form2 = StatsForm2(request.POST or None)
    obj.id_team1 = Team.objects.get(id_team__exact=obj.id_team1).name
    obj.id_team2 = Team.objects.get(id_team__exact=obj.id_team2).name
    ctx = {
        "idTournament": str(id)
    }
    ctx["form1"] = form1
    ctx["form2"] = form2
    ctx["objects"] = obj

    obj = get_object_or_404(Match, id_match__exact=idMatch)

    if request.user.is_authenticated:
        log = LoginData.objects.get(login=request.user.username)
        try:
            organizator = Promoter.objects.get(id_logindata=log.id_logindata)
        except Manager.DoesNotExist:
            organizator = None

        ctx["organizator"] = str(organizator.id_tournament)

    if form1.is_valid() and form2.is_valid() and request.user.is_authenticated:
        passes1 = form1.cleaned_data['passes']
        passes2 = form2.cleaned_data['passes']
        fouls1 = form1.cleaned_data['fouls']
        fouls2 = form2.cleaned_data['fouls']
        ball_position1 = form1.cleaned_data['ball_position']
        ball_position2 = form2.cleaned_data['ball_position']
        strikes1 = form1.cleaned_data['strikes']
        strikes2 = form2.cleaned_data['strikes']
        goals1 = form1.cleaned_data['goals']
        goals2 = form2.cleaned_data['goals']


        print(passes1)
        print(passes2)

        stats1 = Stats(id_team=obj.id_team1, id_match=idMatch, passes=passes1, fouls=fouls1, ball_position=ball_position1, strikes=strikes1, goals=goals1)
        stats2 = Stats(id_team=obj.id_team2, id_match=idMatch, passes=passes2, fouls=fouls2, ball_position=ball_position2, strikes=strikes2, goals=goals2)
        stats1.save()
        stats2.save()
        return redirect('../')

    return render(request, "stats-create.html", ctx)

def stats_view(request, id, idMatch):
    obj = get_object_or_404(Match, id_match__exact=idMatch)
    obj.id_team1 = Team.objects.get(id_team__exact=obj.id_team1).name
    obj.id_team2 = Team.objects.get(id_team__exact=obj.id_team2).name
    try:
        queryset = Stats.objects.filter(id_match__exact=idMatch)
    except:
        queryset = None
    ctx = {
        "objects_list": queryset,
        "teams": obj
    }
    return render(request, "stats.html", ctx)