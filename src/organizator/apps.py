from django.apps import AppConfig


class OrganizatorConfig(AppConfig):
    name = 'organizator'
