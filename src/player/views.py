from django.shortcuts import render, get_object_or_404, redirect
from django.http import HttpResponse

from .forms import PlayerForm, LoginDataForm
from mainApp.models import Player, LoginData, Team, Manager

def players_view(request, idTeam):
    if request.user.is_authenticated:
        log = LoginData.objects.get(login=request.user.username)

        try:
            team_manager = Manager.objects.get(id_logindata__exact=log.id_logindata)
        except:
            team_manager = None
    else:
        team_manager = None
    help = False
    queryset = Player.objects.filter(id_team__exact=idTeam)
    team = Team.objects.get(id_team__exact=idTeam)
    manager = Manager.objects.get(id_team__exact=idTeam)
    if team_manager and team_manager.id_manager==manager.id_manager:
        help = True
    ctx = {
        "object_list": queryset,
        "object": team,
        "manager": manager,
        'help': help
    }
    return render(request, "players-list.html", ctx)
    
def player_detail_view(request, idTeam, id):
    obj = get_object_or_404(Player, id_player=id)
    ctx = {
        "object": obj
    }
    return render(request, "player-detail.html", ctx)

def player_update_view(request, idTeam, id):
    obj = get_object_or_404(Player, id_player=id)
    form = PlayerForm(request.POST or None, instance=obj)
    if form.is_valid():
        form.save()
    ctx = {
        "object": obj,
        "form": form
    }
    return render(request, "player-update.html", ctx)