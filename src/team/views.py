from django.shortcuts import render, redirect, get_object_or_404
from django.contrib import messages
from mainApp.models import LoginData, Player, Team, Tactics, Tournament, Registration, Manager
from .forms import TeamForm

def team_view(request, idTeam):
    obj = get_object_or_404(Team, id_team__exact=idTeam)

    ctx = {
        "object": obj,
    }
    return render(request, "team.html", ctx)

def team_list_view(request, id):
    obj = get_object_or_404(Tournament, id_tournament=id)
    queryset = Team.objects.filter(id_tournament__exact=id)

    ctx = {
        "object_list": queryset,
        "object": obj,
    }
    return render(request, "team-list.html", ctx)

def team_update_view(request, idTeam):
    obj = get_object_or_404(Team, id_team=idTeam)
    tactic = get_object_or_404(Team, id_team=idTeam).id_tactics.tactic
    
    ctx = {
        "object": obj,
        "tactic": tactic
    }
    return render(request, "team-update.html", ctx)

def team_create_view(request):
    form = TeamForm(request.POST or None)
    if form.is_valid() and request.user.is_authenticated:
        log = LoginData.objects.get(login=request.user.username)
        name = form.cleaned_data['name']
        team = Team(name=name)
        team.save()
        manager = Manager.objects.get(id_logindata=log.pk)
        Manager.objects.filter(id_logindata=log.pk).update(id_team=team.pk)
        #organizator.id_tournament = tournament.pk
        #organizator.save()

        return redirect('/manager'+str(manager.id_manager))

    context = {
        "form": form
    }
    return render(request, "team-create.html", context)

def team_registration_view(request, id):
    #form = RegistrationForm(request.POST or None)
    if request.user.is_authenticated:
        log = LoginData.objects.get(login=request.user.username)
        manager = Manager.objects.get(id_logindata=log.pk)
        id_tournament = Tournament.objects.get(id_tournament=id)
        reg = Registration(id_tournament=id_tournament, id_team=manager.id_team, vote=0)
        reg.save()
        messages.success(request, f'-Sent request-')
        return redirect('/tournament'+str(id))

    context = {
        "form": form
    }
    return render(request, "team_registration.html", context)

def delete_registration_view(request, id):
    registration = Registration.objects.get(id_team=id)
    team = Team.objects.get(id_team=id)
    registration.delete()

    team.id_tournament=None
    team.save()
    return redirect('/tournament'+str(registration.id_tournament)+'/petitions')

def update_registration_view(request, id):
    registration = Registration.objects.get(id_team=id)
    registration.vote=True
    registration.save()

    team = Team.objects.get(id_team=id)
    team.id_tournament=registration.id_tournament
    team.save()
    return redirect('/tournament'+str(registration.id_tournament)+'/petitions')

def team_tournament_view(request, id):
    manager = Manager.objects.get(id_manager=id)
    try:
        registration = Registration.objects.get(id_team=manager.id_team)
    except Registration.DoesNotExist:
        registration = None
    if registration is not None:
        tournament = Tournament.objects.get(id_tournament=str(registration.id_tournament))
        ctx = {
           "tournament": tournament,
            "registration": registration
        }
        return render(request, "team_tournament.html", ctx)
    else:
        return render(request, "team_tournament.html")