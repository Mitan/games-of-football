from django import forms

from mainApp.models import Team, Registration

class TeamForm(forms.ModelForm):
    class Meta:
        model = Team
        fields = [
            'name'
        ]

