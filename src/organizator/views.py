from django.shortcuts import render, redirect, get_object_or_404
from django.contrib import messages
from django.contrib.auth import authenticate

from .forms import PromoterForm, TournamentForm
from mainApp.models import Promoter, Tournament, LoginData, Manager, Registration, Team

def organizator_view(request, id):
    obj = get_object_or_404(Promoter, id_promoter=id)
    ctx = {
        "object": obj,
    }
    return render(request, "organizator.html", ctx)

def organizator_update_view(request, id):
    obj = get_object_or_404(Promoter, id_promoter=id)
    form = PromoterForm(request.POST or None, instance=obj)
    if form.is_valid():
        form.save()
    ctx = {
        "object": obj,
        "form": form
    }
    return render(request, "organizator-update.html", ctx)

def organizator_create_view(request):
    form = PromoterForm(request.POST or None)
    if form.is_valid() and request.user.is_authenticated:
        log = LoginData.objects.get(login=request.user.username)    
        name = form.cleaned_data['name']
        surname = form.cleaned_data['surname']
        age = form.cleaned_data['age']
        organizator = Promoter(name=name, surname=surname, age=age, id_logindata=log.pk)
        organizator.save()

        return redirect('/organizator'+str(organizator.id_promoter))

    context = {
        "form": form
    }
    return render(request, "organizator-create.html", context)

def organizator_delete_view(request, id):
    obj = get_object_or_404(Promoter, id_promoter=id)
    if request.method == "POST":
        obj.delete()
        return redirect('/panel/')
    ctx = {
        "object": obj
    }
    return render(request, "organizator-delete.html", ctx)

def tournament_view(request, id):
    obj = get_object_or_404(Tournament, id_tournament=id)
    if request.user.is_authenticated:
        log = LoginData.objects.get(login=request.user.username)
        organizator = Promoter.objects.get(id_tournament=id)
        if log.id_logindata!=organizator.id_logindata:
            organizator = None
        try:
            manager = Manager.objects.get(id_logindata=log.id_logindata)
        except Manager.DoesNotExist:
            manager = None
        if manager is not None and manager.id_team is not None:
            team = Team.objects.get(id_team=str(manager.id_team))
            help = str(obj.id_tournament) == str(team.id_tournament)
        elif manager is not None and manager.id_team is None:
            team = None
            help = "create-team"
        else:
            team = None
            help = None
    else:
        organizator = None
        manager = None
        team = None
        help = None

    ctx = {
        "object": obj,
        "manager": manager,
        "organizator": organizator,
        "team": team,
        "help": help
    }
    return render(request, "tournament.html", ctx)

def tournament_list_view(request, *args, **kwargs):
    queryset = Tournament.objects.all()
    ctx = {
        "object_list": queryset
    }
    return render(request, "tournament-list.html", ctx)

def tournament_create_view(request):
    form = TournamentForm(request.POST or None)
    if form.is_valid() and request.user.is_authenticated:
        log = LoginData.objects.get(login=request.user.username)
        name = form.cleaned_data['name']
        teams_amount = form.cleaned_data['teams_amount']
        tournament = Tournament(name=name, teams_amount=teams_amount)
        tournament.save()
        organizator = Promoter.objects.get(id_logindata=log.pk)
        Promoter.objects.filter(id_logindata=log.pk).update(id_tournament=tournament.pk)
        #organizator.id_tournament = tournament.pk
        #organizator.save()

        return redirect('/organizator'+str(organizator.id_promoter))

    context = {
        "form": form
    }
    return render(request, "tournament-create.html", context)

def petition_view(request, id):
    if request.user.is_authenticated:
        log = LoginData.objects.get(login=request.user.username)
        organizator = Promoter.objects.get(id_tournament=id)
        tournament = Tournament.objects.get(id_tournament=id)
        if log.id_logindata!=organizator.id_logindata:
            messages.success(request, f'O ty oszukisto!')
            return redirect('/organizator'+str(organizator.id_promoter))
        registrations = Registration.objects.filter(id_tournament__exact=id, vote=0)
        queryset = Team.objects.none()
        for x in registrations:
            queryset |= Team.objects.filter(id_team__exact=str(x.id_team))
        ctx = {
            "object_list": queryset,
            "object": tournament
        }
        return render(request, "petition.html", ctx)
    else:
        return render(request, "petition.html")