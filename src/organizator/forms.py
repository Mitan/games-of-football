from django import forms

from mainApp.models import Promoter, Tournament


class PromoterForm(forms.ModelForm):
    class Meta:
        model = Promoter
        fields = [
            'name',
            'surname',
            'age'
        ]

class TournamentForm(forms.ModelForm):
    class Meta:
        model = Tournament
        fields = [
            'name',
            'teams_amount'
        ]