from django.contrib.auth import *
from django.shortcuts import get_object_or_404
from django import forms

from mainApp.models import Manager, LoginData


class ManagerForm(forms.ModelForm):
    id_logindata = forms.IntegerField(label='', widget=forms.HiddenInput(), required=False)
    class Meta:
        model = Manager
        fields = [
            'name',
            'surname',
            'age',
            'id_logindata'
        ]