from django.shortcuts import render, redirect
from django.contrib.auth import login, authenticate
from django.contrib import messages
from .forms import UserRegisterForm, LoginDataForm

from mainApp.models import LoginData

def register(request):
    if request.method == 'POST':
        form = UserRegisterForm(request.POST)
        if form.is_valid():
            form.save()
            username = form.cleaned_data.get('username')
            raw_password = form.cleaned_data.get('password1')
            email = form.cleaned_data.get('email')
            #user = authenticate(username=username, password=raw_password)
            #login(request, user)
            messages.success(request, f'Your account has been created! You are now able to log in.')

            log = LoginData(login=username, password=raw_password, email=email)
            log.save()

            return redirect('/login')
    else:
        form = UserRegisterForm()
    return  render(request, "users/register.html", {'form': form})
