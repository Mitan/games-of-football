from django.shortcuts import render, get_object_or_404, redirect
from django.http import HttpResponse
from django.contrib.auth import authenticate

from .forms import PlayerForm, LoginDataForm
from mainApp.models import Player, LoginData, Manager, Promoter

# def home_view(request, *args, **kwargs):
#     return render(request, "home.html", {})

def home_view(request):
    if request.method == 'POST':
        form = LoginDataForm(request.POST)
        if form.is_valid():
            form.save()
            username = form.cleaned_data.get('username')
            raw_password = form.cleaned_data.get('password')
            user = authenticate(username=username, password=raw_password)
            login(request, user)
            return redirect('home.html')
    else:
        form = LoginDataForm()
    return render(request, 'home.html', {'form': form})

