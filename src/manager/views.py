from django.shortcuts import render, redirect, get_object_or_404
from django.contrib import messages
from django.contrib.auth import authenticate

from .forms import ManagerForm
from mainApp.models import Manager, LoginData


def manager_view(request, id):
    obj = get_object_or_404(Manager, id_manager=id)
    ctx = {
        "object": obj,
    }
    return render(request, "manager.html", ctx)

def manager_update_view(request, id):
    obj = get_object_or_404(Manager, id_manager=id)
    form = ManagerForm(request.POST or None, instance=obj)
    if form.is_valid() and request.user.is_authenticated:
        log = LoginData.objects.get(login=request.user.username)   
        obj.name = form.cleaned_data['name']
        obj.surname = form.cleaned_data['surname']
        obj.age = form.cleaned_data['age']
        obj.id_logindata = log.pk
        obj.save()
    ctx = {
        "object": obj,
        "form": form
    }
    return render(request, "manager-update.html", ctx)
    
def manager_create_view(request):
    form = ManagerForm(request.POST or None)
    if form.is_valid() and request.user.is_authenticated:
        log = LoginData.objects.get(login=request.user.username)    
        name = form.cleaned_data['name']
        surname = form.cleaned_data['surname']
        age = form.cleaned_data['age']
        print("Name " + str(name) + "\nSurname " + str(surname) + "\nAge " + str(age) + "\nId_LoginData " + str(log.pk))
        manager = Manager(name=name, surname=surname, age=age, id_logindata=log.pk)
        manager.save()
        return redirect('/manager'+str(manager.id_manager))

    context = {
        "form": form
    }
    return render(request, "manager-create.html", context)

def manager_delete_view(request, id):
    obj = get_object_or_404(Manager, id_manager=id)
    if request.method == "POST":
        obj.delete()
        return redirect('/panel/')
    ctx = {
        "object": obj
    }
    return render(request, "manager-delete.html", ctx)