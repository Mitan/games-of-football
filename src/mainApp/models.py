from django.db import models


class LoginData(models.Model):
    id_logindata = models.AutoField(primary_key = True)
    login = models.CharField(max_length = 20)
    password = models.CharField(max_length = 30)
    email = models.EmailField(max_length = 30)

    def __str__(self):
        return str(self.id_logindata)


class Person(models.Model):
    name = models.CharField(max_length = 30, default = None)
    surname = models.CharField(max_length = 30, default = None)
    age = models.PositiveSmallIntegerField(default = None)

    class Meta:
        abstract = True


class Tournament(models.Model):
    id_tournament = models.AutoField(primary_key=True)
    name = models.CharField(max_length=40)
    teams_amount = models.IntegerField()

    def __str__(self):
        return str(self.id_tournament)


class Tactics(models.Model):
    id_tactics = models.AutoField(primary_key=True)
    tactic = models.CharField(max_length=50)

    def __str__(self):
        return str(self.id_tactics)


class Team(models.Model):
    id_team = models.AutoField(primary_key = True)
    id_tactics = models.ForeignKey(Tactics, on_delete = models.DO_NOTHING, null=True, blank=True)
    id_tournament = models.ForeignKey(Tournament, on_delete = models.DO_NOTHING, null=True, blank=True)
    name = models.CharField(max_length = 30)

    def __str__(self):
        return str(self.id_team)


class Promoter(Person):
    id_promoter = models.AutoField(primary_key = True)
    id_logindata = models.IntegerField(null=True, blank=True)
    id_tournament = models.ForeignKey(Tournament, on_delete= models.CASCADE, null=True, blank=True)

    def __str__(self):
        return str(self.id_promoter)

class Manager(Person):
    id_manager = models.AutoField(primary_key = True)
    id_logindata = models.IntegerField(null=True, blank=True)
    id_team = models.ForeignKey(Team, on_delete = models.CASCADE, null=True, blank=True)

    def __str__(self):
        return str(self.id_manager)


class Standings(models.Model):
    id_tournament = models.ForeignKey(Tournament, on_delete = models.DO_NOTHING)
    id_team = models.OneToOneField(Team, primary_key = True, on_delete = models.DO_NOTHING)
    place = models.CharField (max_length = 50)
    points = models.PositiveIntegerField(default = 0)
    goals = models.PositiveIntegerField(default = 0)
    y_card_number = models.PositiveIntegerField(default = 0)
    r_card_number = models.PositiveIntegerField(default = 0)


class Stats(models.Model):
    id_stats = models.AutoField(primary_key = True)
    id_team = models.IntegerField()
    id_match = models.IntegerField()
    ball_position = models.PositiveIntegerField(default = 0)
    passes = models.PositiveIntegerField(default = 0)
    fouls = models.PositiveIntegerField(default = 0)
    strikes = models.PositiveIntegerField(default = 0)
    goals = models.PositiveIntegerField(default = 0)


class Match(models.Model):
    id_match = models.AutoField(primary_key = True)
    id_tournament = models.ForeignKey(Tournament, on_delete = models.DO_NOTHING)
    id_team1 = models.PositiveIntegerField()
    id_team2 = models.PositiveIntegerField()
    id_stats = models.ForeignKey(Stats, on_delete = models.DO_NOTHING, blank=True, null=True)
    date = models.DateField()
    localization = models.CharField(max_length = 30)


class Player(Person):
    id_player = models.AutoField(primary_key = True)
    height = models.PositiveSmallIntegerField()
    weight = models.PositiveSmallIntegerField()
    POSITIONS = [
        ('GK', 'Goalkeeper'),
        ('LB', 'Left back'),
        ('RB', 'Right back'),
        ('CB', 'Center back'),
        ('LM', 'Left midfielder'),
        ('RM', 'Right midfielder'),
        ('CM', 'Center midfielder'),
        ('CDM', 'Center defensive midfielder'),
        ('CAM', 'Center attacking midfielder'),
        ('RW', 'Right winger'),
        ('LW', 'Left winger'),
        ('ST', 'Striker'),
    ]
    position = models.CharField(max_length = 3, choices = POSITIONS)
    BETTER_FOOT = [
        ('L', 'Left'),
        ('R', 'Right')
    ]
    better_foot = models.CharField(max_length = 1, choices = BETTER_FOOT)
    tricks = models.PositiveIntegerField()
    overall = models.PositiveIntegerField()
    STATUS = [
        ('F', 'First Squad'),
        ('B', 'Bench'),
        ('R', 'Reserve'),
    ]
    status = models.CharField(max_length = 1, choices = STATUS)
    id_team = models.ForeignKey(Team, on_delete = models.DO_NOTHING)

    def get_absolute_url(self):
        return reverse("products:product-detail", kwargs={"id": self.id})

    def __str__(self):
        return self.name + ' ' + self.surname


class Ranking(models.Model):
    id_ranking = models.AutoField(primary_key = True)
    id_player = models.OneToOneField(Player, on_delete = models.DO_NOTHING)
    id_match = models.ForeignKey(Match, on_delete = models.DO_NOTHING)
    fouls = models.IntegerField()
    y_card_number = models.IntegerField()
    r_card_number = models.IntegerField()
    nutmeg = models.IntegerField()
    goals = models.IntegerField()


class Registration(models.Model):
    id_tournament = models.ForeignKey(Tournament, on_delete = models.DO_NOTHING)
    id_team = models.OneToOneField(Team, primary_key = True, on_delete = models.DO_NOTHING)
    vote = models.BooleanField()

    def __str__(self):
        return str(self.id_tournament)
    