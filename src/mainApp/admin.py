from django.contrib import admin
from .models import LoginData, Tournament, Tactics, Team, Promoter, Manager, Standings, Stats, Ranking, Player, Match, Registration

admin.site.register(LoginData)
admin.site.register(Tournament)
admin.site.register(Tactics)
admin.site.register(Team)
admin.site.register(Promoter)
admin.site.register(Manager)
admin.site.register(Standings)
admin.site.register(Stats)
admin.site.register(Ranking)
admin.site.register(Player)
admin.site.register(Match)
admin.site.register(Registration)